<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['name', 'description', 'price', 'start_date', 'end_date', 'done_tasks'];

    protected $dates = ['start_date', 'end_date'];

    public function setUsersAttribute($value)
    {
        $this->users()->sync($value);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function getUserIdsAttribute()
    {
        return $this->users->pluck('id')->toArray();
    }

    public function getStartDateAttribute($value)
    {
        $time = new Carbon($value);
        return $time->format('Y-m-d');
    }

    public function getEndDateAttribute($value)
    {
        $time = new Carbon($value);
        return $time->format('Y-m-d');
    }
}
