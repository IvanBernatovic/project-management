<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'user_ids.*' => 'integer|exists:users,id',
        ]);

        $data = $request->all();
        $project = auth()->user()->owned_projects()->create($data);
        if(isset($data['user_ids']) && is_array($data['user_ids']))
            $project->users()->sync($data['user_ids']);

        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        if(auth()->user()->isOwnerOf($project)){
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required|numeric',
                'description' => 'required',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'user_ids.*' => 'integer|exists:users,id',
            ]);
        }

        $data = $request->all();

        $project->update($data);
        if(isset($data['user_ids']) && is_array($data['user_ids']))
            $project->users()->sync($data['user_ids']);

        return redirect()->route('project.edit', $project);
    }
}
