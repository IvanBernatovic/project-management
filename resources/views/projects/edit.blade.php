@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit project</div>

                    <div class="panel-body">
                        {!! Form::model($project, ['route' => ['project.update', $project], 'method' => 'PUT']) !!}

                        @if(auth()->user()->isOwnerOf($project))
                            <div class="form-group">
                                <label for="name">Name</label>
                                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'required']) !!}
                            </div>

                            <div class="form-group">
                                <label for="price">Price</label>
                                {!! Form::number('price', null, ['class' => 'form-control', 'id' => 'price', 'step' => 0.01, 'required']) !!}
                            </div>

                            <div class="form-group">
                                <label for="user_ids">Users</label>
                                {!! Form::select('user_ids[]', \App\User::where('id', '<>', auth()->user()->id)->get()->pluck('name', 'id'), $project->user_ids, ['class' => 'form-control','multiple' => true]) !!}
                            </div>

                            <div class="form-group">
                                <label for="start_date">Start date</label>
                                {!! Form::date('start_date', null, ['class' => 'form-control', 'id' => 'start_date', 'required']) !!}
                            </div>

                            <div class="form-group">
                                <label for="end_date">End date</label>
                                {!! Form::date('end_date', null, ['class' => 'form-control', 'id' => 'end_date', 'required']) !!}
                            </div>

                        @else
                            <dl class="dl-horizontal">
                                <dt>Name</dt>
                                <dd>{{ $project->name }}</dd>

                                <dt>Price</dt>
                                <dd>{{ $project->price }}</dd>

                                <dt>Description</dt>
                                <dd>{{ $project->description }}</dd>

                                <dt>Owner</dt>
                                <dd>{{ $project->owner->name }}</dd>

                                <dt>Start date</dt>
                                <dd>{{ $project->start_date }}</dd>

                                <dt>End date</dt>
                                <dd>{{ $project->end_date }}</dd>

                                <dt>Users</dt>
                                <dd>
                                    <ul>
                                        @foreach($project->users as $user)
                                        <li>{{ $user->name }}</li>
                                        @endforeach
                                    </ul>
                                </dd>
                            </dl>
                        @endif

                        <div class="form-group">
                            <label for="done_tasks">Done tasks</label>
                            {!! Form::textarea('done_tasks', null, ['class' => 'form-control', 'id' => 'done_tasks']) !!}
                        </div>

                        <div class="form-group">
                            <div class="clearfix">

                                <div class="pull-right">
                                    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
