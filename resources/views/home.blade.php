@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <div>
                        <a href="{{ route('project.create') }}" class="btn btn-primary">Add new project</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Projects</div>

            <div class="panel-body">
                <div class="col-md-6">
                    <h3>My projects</h3>

                    <ul>
                        @foreach(auth()->user()->owned_projects as $project)
                            <li><a href="{{ route('project.edit', $project) }}">{{ $project->name }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-md-6">
                    <h3>Projects I'm part of</h3>

                    <ul>
                        @foreach(auth()->user()->projects as $project)
                            <li><a href="{{ route('project.edit', $project) }}">{{ $project->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
